﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{

    public class DAL
    {

        public static List<User> getAllUsers()
        {
            List<User> list = new List<User>();
           

            string connStr = @"Data Source=.\SQLEXPRESS2012;initial Catalog=Assignment4;User Id=sa;Password=123;";
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();
                string q = string.Format("select * from [Assignment4].[dbo].[User] ");
                SqlCommand command = new SqlCommand(q, conn);
                SqlDataReader sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    User u = new User();
                    u.id = sdr.GetInt32(sdr.GetOrdinal("UserID"));
                    u.login = sdr.GetString(sdr.GetOrdinal("Login"));
                    u.name = sdr.GetString(sdr.GetOrdinal("Name"));
                    u.img = sdr.GetString(sdr.GetOrdinal("imageName"));
                    u.pass = sdr.GetString(sdr.GetOrdinal("Password"));
                    u.gender = sdr.GetString(sdr.GetOrdinal("gender"));
                    u.address = sdr.GetString(sdr.GetOrdinal("address"));
                    u.NIC = sdr.GetString(sdr.GetOrdinal("NIC"));
                    u.dob = Convert.ToDateTime(sdr.GetDateTime(sdr.GetOrdinal("DOB")));
                    u.age = sdr.GetInt32(sdr.GetOrdinal("age"));
                    u.cricket = (sdr.GetBoolean(sdr.GetOrdinal("isCricket"))) ? 1 : 0;
                    u.Hockey = (sdr.GetBoolean(sdr.GetOrdinal("Hockey"))) ? 1 : 0;
                    u.chess = (sdr.GetBoolean(sdr.GetOrdinal("Chess"))) ? 1 : 0;
                    list.Add(u);
                   
                }
            }

            
            return list;
        }
        public static Boolean sendEmail(string email, string subject, string body)
        {
            try
            {
                string fromDisplayEmail = "eadsef15morning@gmail.com";
                string fromPassword = "EAD_sef15";
                string fromDisplayName ="ResetPassword";
                MailAddress fromAddress = new MailAddress(fromDisplayEmail, fromDisplayName);
                MailAddress toAddress = new MailAddress(email);
                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress) {
                    Subject = subject, Body = body
                })
                {
                    smtp.Send(message);
                }
                return true;
            }catch(Exception ex)
            {
                var x = ex;
                return false;
            }
        }
        public static bool authenticateAdmin(string login, string pass)
        {

            bool flag = false;
            string connStr = @"Data Source=.\SQLEXPRESS2012;initial Catalog=Assignment4;User Id=sa;Password=123;";
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();
                string q = string.Format("select [Login] , [Password] from [Assignment4].[dbo].[Admin] where Login='{0}' AND Password='{1}'", login, pass);
                SqlCommand command = new SqlCommand(q, conn);
                SqlDataReader sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    flag = true;
                    string loginn = sdr.GetString(sdr.GetOrdinal("login"));
                    string passs = sdr.GetString(sdr.GetOrdinal("password"));

                }
            }

            return flag;
        }
        public static bool authenticate(string login, string pass)
        {

            bool flag = false;
            string connStr = @"Data Source=.\SQLEXPRESS2012;initial Catalog=Assignment4;User Id=sa;Password=123;";
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();
                string q = string.Format("select [Login] , [Password] from [Assignment4].[dbo].[User] where Login='{0}' AND Password='{1}'", login, pass);
                SqlCommand command = new SqlCommand(q, conn);
                SqlDataReader sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    flag = true;
                    string loginn = sdr.GetString(sdr.GetOrdinal("login"));
                    string passs = sdr.GetString(sdr.GetOrdinal("password"));

                }
            }

            return flag;
        }

       public static User getUserbyLogin(string login)
        {
            User u = new User();
            string connString = @"Data Source=.\SQLEXPRESS2012;initial Catalog=Assignment4 ;User Id=sa ; Password=123;";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                string q = string.Format(@"select * FROM [Assignment4].[dbo].[User] where Login='{0}'", login);

                SqlCommand command = new SqlCommand(q, conn);

               SqlDataReader sdr = command.ExecuteReader();
                while(sdr.Read())
                {
                    u.name = sdr.GetString(sdr.GetOrdinal("Name"));
                    u.img = sdr.GetString(sdr.GetOrdinal("imageName"));
                    u.pass = sdr.GetString(sdr.GetOrdinal("Password"));
                    u.gender = sdr.GetString(sdr.GetOrdinal("gender"));
                    u.address = sdr.GetString(sdr.GetOrdinal("address"));
                    u.NIC = sdr.GetString(sdr.GetOrdinal("NIC"));
                    u.dob = Convert.ToDateTime(sdr.GetDateTime(sdr.GetOrdinal("DOB")));
                    u.age = sdr.GetInt32(sdr.GetOrdinal("age"));
                    u.cricket = (sdr.GetBoolean(sdr.GetOrdinal("isCricket")))? 1:0;
                    u.Hockey = (sdr.GetBoolean(sdr.GetOrdinal("Hockey")))? 1:0;
                    u.chess = (sdr.GetBoolean(sdr.GetOrdinal("Chess")))? 1:0;
                }

            }

                return u;
        }
        public static int UpdateUser(User u)
        {
         
            string connString = @"Data Source=.\SQLEXPRESS2012;initial Catalog=Assignment4 ;User Id=sa
                                  ; Password=123;";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                string q = string.Format(@"UPDATE [Assignment4].[dbo].[User] SET Name='{0}', 
                                                Password='{1}' , gender='{2}', Address='{3}',
                                            Age='{4}' , DOB='{5}', NIC='{6}' , IsCricket={7},
                                                Hockey={8}, Chess={9}, ImageName='{10}' where Login='{11}'"
                                                , u.name, u.pass, u.gender, u.address, u.age,
                                                u.dob.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                                                u.NIC, u.cricket, u.Hockey, u.chess, u.img, u.login);

                SqlCommand command = new SqlCommand(q, conn);

                int s = command.ExecuteNonQuery();
                if (s != 0)
                    return s;
                else
                    return 0;
            }


          

        }
        public static int SaveUser(User u)
        {
            int flag = 0;
            string connString = @"Data Source=.\SQLEXPRESS2012;
        initial Catalog=Assignment4 ;User Id=sa ; Password=123;";
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                string q = string.Format(@"select [Password] FROM [Assignment4].[dbo].[User] where Login='{0}'"
                            , u.login);

                SqlCommand command = new SqlCommand(q, conn);

                string s = (string)command.ExecuteScalar();
                if (s != null)
                    flag = 2;

                string uid = "DEFAULT";
                if (flag == 0)
                {
                    q = string.Format(@"INSERT into [Assignment4].[dbo].[User] (Name,Login,Password,
NIC,Hockey,IsCricket,Chess,DOB,gender,age,address,ImageName,createdOn)

Values('{0}','{1}','{2}','{3}',{4},{5},{6},'{7}','{8}',{9},'{10}','{11}','{12}')",
u.name, u.login, u.pass, u.NIC, u.Hockey, u.cricket, u.chess, u.dob.ToString("yyyy-MM-dd HH:mm:ss.fff"),
u.gender, u.age, u.address, u.img, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));


                    command = new SqlCommand(q, conn);
                    int i = command.ExecuteNonQuery();
                    if (i != 0) { flag = 1; }


                }


            }
            return flag;


        }

    }
}
