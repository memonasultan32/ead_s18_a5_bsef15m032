﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class User
    {
        public string img { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string login { get; set; }
        public string pass { get; set; }
        public string address { get; set; }

        public string gender { get; set; }
        public int age { get; set; }
        public string NIC { get; set; }
        public DateTime dob { get; set; }
        public int cricket { get; set; }

        public int Hockey { get; set; }

        public int chess { get; set; }
    }

}
