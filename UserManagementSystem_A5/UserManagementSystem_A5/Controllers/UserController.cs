﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace UserManagementSystem_A5.Controllers
{
  
    public class UserController : Controller
    {
        private static string code;
        Random r = new Random();


        public ActionResult Code()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Code")]
        public ActionResult Code2()
        {
            if(Request["codeBtn"]!=null)
            {
                string abc = Request["code"];
                if(abc==code.ToString())
                {
                    ViewBag.login = Session["user"];
                    return View("Reset");
                }
            }
            
                return View();
        }
        public ActionResult Reset()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Reset")]
        public ActionResult Reset2()
        {
            if(Request["change"]!=null)
            {

                string login = Request["txtLogin"];
                User u = DAL.DAL.getUserbyLogin(login);
                u.login = login;
                u.pass = Request["txtPassword"];
                if(DAL.DAL.UpdateUser(u)==1)
                {
                    ViewBag.username = u.name;
                    ViewBag.img = u.img;
                    ViewBag.login = u.login;
                    return View("UserHome");
                }
            }
                return View();
        }

        /*____________________________________________________________*/
        // GET: User
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult Index2()
        {
            if (Request["newUser"] != null)
            {
                ViewBag.action = "Create";
                return View("NewUser");
            }
            else if (Request["exUser"] != null)
                return Redirect("LoginScreen");
            else if (Request["admin"] != null)
                return Redirect("AdminLogin");
            else
                return View();
        }
       
        [HttpGet]
        public ActionResult AdminHome()
        {
            if(Session["admin"]!=null)
            {
                List<User> arr = new List<User>();
                arr = DAL.DAL.getAllUsers();
                ViewBag.arr = arr;
                return View("AdminHome");
            }
            return View("AdminLogin");
        }

        [HttpPost]
        [ActionName("AdminHome")]
        public ActionResult AdminHome2()
        {
            if (Request["edit"] != null)
            {

                string login = Request["login"];
                Session["admin"] = login;
                User u = DAL.DAL.getUserbyLogin(Request["login"]);
                ViewBag.action = "Save Changes";
                ViewBag.img = u.img;
                ViewBag.name = u.name;
                ViewBag.login = login;
                ViewBag.pass = u.pass;
                if (u.gender == "M")
                    ViewBag.male = "checked";
                else
                    ViewBag.female = "checked";
                ViewBag.address = u.address;
                ViewBag.nic = Convert.ToString(u.NIC);
                ViewBag.dob = u.dob.ToString("yyyy-MM-dd");
                ViewBag.age = u.age;
                ViewBag.cricket = (u.cricket == 1) ? "checked" : " ";
                ViewBag.hockey = (u.Hockey == 1) ? "checked" : " ";
                ViewBag.chess = (u.chess == 1) ? "checked" : " ";

                return View("NewUser");
            }
            return View();
        }

        [HttpGet]
        public ActionResult AdminLogin()
        {
            Session["admin"] = null;
            return View();
        }

        [HttpPost]
        [ActionName("AdminLogin")]
        public ActionResult AdminLogin2()
        {
            if (Request["btnLogin"] != null)
            {

                if (Request["txtLogin"] == "" || Request["txtPassword"] == "")
                {
                    ViewBag.error = "Enter all fields please!";
                    return View();
                }
                var login = Request["txtLogin"];
                var pass = Request["txtPassword"];
                if (DAL.DAL.authenticateAdmin(login, pass))
                {
                    Session["admin"] = login;
                    List<User> arr = new List<User>();
                    arr = DAL.DAL.getAllUsers();
                    ViewBag.arr = arr;  
                    return View("AdminHome");
                }
                else
                {
                    ViewBag.error = "No such admin exists";
                    return View();
                }
            }
            
            else if(Request["cancel"] != null)
            {
                Session["admin"] = null;
                return Redirect("Index");
            }
         
            return View();
        }

        [HttpGet]
        public ActionResult LoginScreen()
        {
            if(Request["logout"]!=null)
            {
                Session["login"] = null;
            }
            
            return View();
        }

        [HttpPost]
        [ActionName("LoginScreen")]
        public ActionResult LoginScreen2()
        {
            if (Request["btnLogin"] != null)
            {
                
                if (Request["txtLogin"] == null || Request["txtPassword"] == null)
                { 
                    ViewBag.error = "Enter all fields please!";
                    return View();
                }
                var login = Request["txtLogin"];
                var pass = Request["txtPassword"];
                if (DAL.DAL.authenticate(login, pass))
                {
                    User u = DAL.DAL.getUserbyLogin(login);
                    ViewBag.username = u.name;
                    ViewBag.img = u.img;
                    ViewBag.login = login;
                    return View("UserHome");
                }
                else
                {

                    ViewBag.error = "No such user exists";
                    return View();
                }
            }
            else if(Request["reset"]!=null)
            {
                if (Request["txtLogin"] == null || Request["email"] == null)
                {
                    ViewBag.error = "Enter login and email fields please!";
                    return View();
                }
                string Email = Request["email"];
                string login = Request["txtLogin"];
                string subject = "ResetPassword";
                
                code = r.Next(1000, 10000).ToString(); // 4 digit password
                string body = code;
                if (DAL.DAL.sendEmail(Email, subject, body))
                {
                    Session["user"] = login;
                    return View("Code");
                }  
            }

            return Redirect("Index");
        }

        [HttpGet]
        public ActionResult NewUser()
        {
            return Redirect("NewUser");
        }

        [HttpPost]
        [ActionName("NewUser")]
        public ActionResult NewUser2()
        {
            // validation
            if (Request["create"] == "Create" || Request["create"] == "Submit")
            {
                bool flag = true;
                if (Request["male"] != null || Request["female"] != null)
                {
                    if (Request["chess"] != null || Request["cricket"] != null || Request["hockey"] != null)
                    {
                        if (Request["name"] == null || Request["password"] == null || Request["address"] == null || Request["NIC"] == "" || Request["dob"] == "0001-01-01" ||
                            Request["age"] == "")
                            flag = false;
                    }
                    else
                        flag = false;                    
                }
                else                   
                    flag = false;
                if (flag == false)
                {
                    ViewBag.error = "Enter all fields please!";
                    return View();
                }
                // validation ends
                User u = new User();
                u.login = Request["login"];
                var file = Request.Files["filename"];

                if (file.FileName != "")
                {
                    u.img = file.FileName;
                    var rootPath = Server.MapPath("~/Images");
                    var fileSavePath = System.IO.Path.Combine(rootPath, u.img);
                    file.SaveAs(fileSavePath);
                }
                u.name = Request["name"];
                u.pass = Request["password"];
                u.gender = (Request["male"] == null) ? "F" : "M";
                u.address = Request["address"];
                u.NIC = Request["NIC"];
                u.dob = Convert.ToDateTime(Request["dob"]);
                u.age = Convert.ToInt32(Request["age"]);
                u.cricket = (Request["cricket"] != null) ? 1 : 0;
                u.Hockey = (Request["hockey"] != null) ? 1 : 0;
                u.chess = (Request["chess"] != null) ? 1 : 0;

                int choice = DAL.DAL.SaveUser(u);
                if (choice == 1)
                {

                    ViewBag.username = u.name;
                    ViewBag.img = u.img;
                    ViewBag.login = u.login;
                    return View("UserHome");
                }
                else if (choice == 2)
                {
                    ViewBag.error = "User Already Exists";
                    return View();
                }
                else
                {
                    ViewBag.error = "Some error occured";
                    return View();
                }

                
            }
            else if (Request["create"] == "Save Changes")
            {

                // validation
                bool flag = true;
                if (Request["male"] != null || Request["female"] != null)
                {
                    if (Request["chess"] != null || Request["cricket"] != null || Request["hockey"] != null)
                    {
                        if (Request["name"] == null || Request["password"] == null || Request["address"] == null || Request["NIC"] == "" || Request["dob"] == "0001-01-01" ||
                            Request["age"] == "")
                            flag = false;
                    }
                    else
                        flag = false;
                }
                else
                    flag = false;
                if (flag == false)
                {
                    ViewBag.error = "Enter all fields please!";
                    return View();
                }
                // validation ends
                string login = Request["login"];
                    Session["login"] = login;
                    User u = DAL.DAL.getUserbyLogin(login);
                    u.login = login;
                    // image saving and uploading here 
                    var file = Request.Files["filename"];
                    if (file.FileName != "")
                    {
                        u.img = file.FileName;
                        var rootPath = Server.MapPath("~/Images");
                        var fileSavePath = System.IO.Path.Combine(rootPath, u.img);
                        file.SaveAs(fileSavePath);
                    }

                    u.name = Request["name"];

                    u.pass = Request["password"];
                    u.gender = (Request["male"] == null) ? "F" : "M";
                    u.address = Request["address"];
                    u.NIC = Request["NIC"];
                    u.dob = Convert.ToDateTime(Request["dob"]);
                    u.age = Convert.ToInt32(Request["age"]);
                    u.cricket = (Request["cricket"] != null) ? 1 : 0;
                    u.Hockey = (Request["hockey"] != null) ? 1 : 0;
                    u.chess = (Request["chess"] != null) ? 1 : 0;

                    int choice = DAL.DAL.UpdateUser(u);
                    if (choice != 0)
                    {
                        ViewBag.error = "Changes Saved Successfully!";
                    }
                    else
                    {
                        ViewBag.error = "Try Again!";
                    }
                    if (Session["admin"] != null)
                        return Redirect("AdminHome");
                    else if (Session["login"] != null)
                    {
                        User u2 = DAL.DAL.getUserbyLogin((string)Session["login"]);
                        ViewBag.username = u2.name;
                        ViewBag.img = u2.img;
                        ViewBag.login = (string)Session["login"];
                        return View("UserHome");
                    }
                    else
                        return View("Index");

                }
                else
                {
                    if (Session["admin"] != null)
                        return Redirect("AdminHome");
                    else if (Session["login"] != null)
                    {

                        return Redirect("UserHome");
                    }
                    else
                        return Redirect("Index");
                }
            }
        

        [HttpGet]
        public ActionResult UserHome()
        {
            if (Session["login"] ==null)
                return Redirect("~/User/LoginScreen");

            User u2 = DAL.DAL.getUserbyLogin((string)Session["login"]);
            ViewBag.username = u2.name;
            ViewBag.img = u2.img;
            ViewBag.login = (string)Session["login"];
            return View();
        }

        [HttpPost]
        [ActionName("UserHome")]
        public ActionResult UserHome2()
        {
            
            if (Request["edit"] != null)
            {
            
                string login = Request["login"];
                Session["login"] = login;
                User u= DAL.DAL.getUserbyLogin(Request["login"]);
                ViewBag.action = "Save Changes";
                ViewBag.img = Request["image"];
                ViewBag.name = u.name;
                ViewBag.login = login;
                ViewBag.pass = u.pass;           
                if(u.gender=="M")
                    ViewBag.male = "checked";
                else
                    ViewBag.female = "checked";
                ViewBag.address = u.address;
                ViewBag.nic = Convert.ToString(u.NIC);
                ViewBag.dob = u.dob.ToString("yyyy-MM-dd");
                ViewBag.age = u.age;
                ViewBag.cricket = (u.cricket==1)? "checked" : " ";
                ViewBag.hockey = (u.Hockey == 1)? "checked" : " ";
                ViewBag.chess = (u.chess == 1)? "checked" : " ";

                return View("NewUser");
            }
            else
            {
                Session["login"] = null;
                return Redirect("~/User/Index");
            }
          
        }
    }
}